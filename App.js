/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  StyleSheet, 
  TouchableOpacity, 
  Text, 
  View, 
  Image,
  TextInput,
} from  'react-native';

import Message from './app/components/message/Message';


const provincias = [
  {
    id:1,
    name: 'Arequipa',
  },
  {
    id:2,
    name: 'Puno',
  },
  {
    id:3,
    name: 'Cuzco',
  },
];

export default class App extends Component{
  constructor(props){
    super(props);
    this.state = {
      textValue:'',
      count:0,
    };
  }
  
  changeTextInput = text =>{

    console.log(text)
    this.setState({textValue: text});
  };
  
  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return(
      <View style={styles.container}>
        <View style={styles.text}>

        <Message/>
        
          <Text> Ingrese su Edad </Text>
        </View>

        

        <TextInput 
          style={{ height: 40, borderColor:'gray', borderWidth:1}}
          onChangeText={text => this.changeTextInput(text)}
          value={this.state.textValue}
          
        />

        <Text>Texto en Body</Text>

        <TouchableOpacity style={styles.button} onPress={this.onPress}>
          <Text> Touch Here </Text>
        </TouchableOpacity>
        <View style={[styles.countContainer]}>
          <Text style={[styles.countText]}>
            {this.state.count}
          </Text>
        </View>

        {provincias.map(item =>(
          <View>
            <Text>{item.name}</Text>
          </View>
      ))}

      </View>

    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top:10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});
